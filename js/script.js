let userNumber;

do {
    userNumber = +prompt('Enter a number greater than zero');
} while (validateValue(userNumber));

if (userNumber && userNumber < 5) {
    console.log('Sorry, no numbers');        
} else {
    for (let i = 0; i <= userNumber; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
}

// advansed task

let num1, num2;

do {
    num1 = +prompt('Enter first number');
    num2 = +prompt('Enter second number');
} while (validateValue(num1) || validateValue(num2) || num1 >= num2);

function isPrime(num) {
    if (num <= 1) {
        return false;
    }
    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
            return false;
        }
    }
    return true;
}

function printPrimes(first, second) {
    for (let i = first; i <= second; i++) {
        if (isPrime(i)) {
            console.log(i);
        }
    }
}

printPrimes(num1, num2);

// validation

function validateValue(value) {
    return !Number.isInteger(value) || value % 1 !== 0 || !value || value < 0;
}